package Home.integrate;

public interface Functional {
    double f(double x);
}