package Home.integrate;

public class HelloIntegrate {

    static double n = 0.1;

    public static double integrate(Functional fct, double a, double b) {
        double LeftTriangle = (b - a) / n;
        double x;
        double sum = 0.0;
        for (int i = 0; i < n; i++) {
           x = (i - 1.0) / n;

            sum += fct.f(x) * LeftTriangle;
        }
        return sum;
    }

}
